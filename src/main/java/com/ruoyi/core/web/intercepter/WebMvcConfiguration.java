package com.ruoyi.core.web.intercepter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {
    @Autowired
    private SystemListInterceptor systemListInterceptor;
    //配置拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
    	
    	List<String> urls = new ArrayList<String>();
    	urls.add("/system/user/list");
    	urls.add("/system/dict/list");
    	urls.add("/system/dict/data/list");
    	urls.add("/system/post/list");
    	urls.add("/system/config/list");
    	urls.add("/system/notice/list");
    	urls.add("/system/operlog/list");
    	urls.add("/system/logininfor/list");
    	urls.add("/system/role/list");
    	urls.add("/monitor/online/list");
    	urls.add("/monitor/job/list");
    	urls.add("/monitor/jobLog/list");
    	
    	urls.add("/monitor/logininfor/list");
    	urls.add("/monitor/operlog/list");
    	urls.add("/tool/gen/list");
    	urls.add("/tool/gen/db/list");
    	
    	urls.add("/system/role/authUser/allocatedList");
    	urls.add("/system/role/authUser/unallocatedList");
    	
    	
        registry.addInterceptor(systemListInterceptor).addPathPatterns(urls);
    }
    
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
    	MappingJackson2HttpMessageConverter jackson2HttpMessageConverter = null;
    	for (HttpMessageConverter<?> converter : converters) {
            if (converter instanceof MappingJackson2HttpMessageConverter){
            	jackson2HttpMessageConverter = (MappingJackson2HttpMessageConverter) converter;
            	break;
            }
        }
    	if(jackson2HttpMessageConverter == null){
    		jackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
    	}
        ObjectMapper objectMapper = new ObjectMapper();
        
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        objectMapper.registerModule(simpleModule);
        jackson2HttpMessageConverter.setObjectMapper(objectMapper);
        converters.add(jackson2HttpMessageConverter);

    }
}